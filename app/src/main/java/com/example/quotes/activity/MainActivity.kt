package com.example.quotes.activity

import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import android.os.Bundle
import android.widget.ImageView
import com.example.quotes.R
import com.example.quotes.QuotesDb
import com.example.quotes.adapter.ListQuotesAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import java.util.*

class MainActivity : AppCompatActivity(), ListQuotesAdapter.OnItemClick {
    var tvLove: TextView? = null
    var tvMotivation: TextView? = null
    var tvAttitude: TextView? = null
    var tvSad: TextView? = null
    var tvAngry: TextView? = null
    var llMore: LinearLayoutCompat? = null
    private var rvTop: RecyclerView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
    }

    private fun initView() {
        tvLove = findViewById(R.id.tv_love)
        tvMotivation = findViewById(R.id.tv_motivation)
        tvAttitude = findViewById(R.id.tv_attitude)
        tvSad = findViewById(R.id.tv_sad)
        tvAngry = findViewById(R.id.tv_angry)
        llMore = findViewById(R.id.ll_more)
        rvTop = findViewById(R.id.rv_top_of_the_day)
        val quotesDb = QuotesDb(this, "quotes.db")
        quotesDb.createDatabse()
        val list = quotesDb.quotes


        list.shuffle()

        val selected = list.subList(0, 30)

        val adapter = ListQuotesAdapter(this, this)
        adapter.setList(selected)


        rvTop?.adapter = adapter
        rvTop?.layoutManager = LinearLayoutManager(this)

    }

    companion object {
        private const val TAG = "MainActivity"
    }

    override fun onCopyClick(quotes: String) {

    }

    override fun onShareClick(s: String) {

    }

    override fun onFavClick(ivFav: ImageView, id: Int) {

    }
}