package com.example.quotes.adapter

import android.R.attr
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import com.example.quotes.R
import com.example.quotes.databinding.LayoutRvListQuotesBinding
import android.R.attr.button
import android.graphics.Color

import androidx.core.graphics.drawable.DrawableCompat

import android.graphics.drawable.Drawable


class ListQuotesAdapter(val context: Context, val listener: OnItemClick) :
    RecyclerView.Adapter<ListQuotesAdapter.Holder>() {
    var quotesList: List<QuotesModel>? = null


    interface OnItemClick {
        fun onCopyClick(quotes: String)
        fun onShareClick(s: String)
        fun onFavClick(ivFav: ImageView, id: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding = LayoutRvListQuotesBinding.inflate(LayoutInflater.from(context), parent, false)
        return Holder(binding)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        var color = ContextCompat.getColor(context, R.color.colorOrange)
        var iconColor = ContextCompat.getColor(context, R.color.colorOrangeDark)
        when {
            holder.adapterPosition % 4 == 0 -> {
                color = ContextCompat.getColor(context, R.color.colorOrange)
                iconColor = ContextCompat.getColor(context, R.color.colorOrangeDark)
            }
            holder.adapterPosition % 4 == 1 -> {
                color = ContextCompat.getColor(context, R.color.colorPink)
                iconColor = ContextCompat.getColor(context, R.color.colorPinkDark)
            }
            holder.adapterPosition % 4 == 2 -> {
                color = ContextCompat.getColor(context, R.color.colorGreen)
                iconColor = ContextCompat.getColor(context, R.color.colorGreenDark)
            }
            holder.adapterPosition % 4 == 3 -> {
                color = ContextCompat.getColor(context, R.color.colorBlue)
                iconColor = ContextCompat.getColor(context, R.color.colorBlueDark)
            }
        }


        var buttonDrawable: Drawable = holder.binding.root.background
        buttonDrawable = DrawableCompat.wrap(buttonDrawable)
        DrawableCompat.setTint(buttonDrawable, color)
        holder.binding.root.background = buttonDrawable


        holder.binding.ivCopy.setColorFilter(iconColor, android.graphics.PorterDuff.Mode.MULTIPLY);
        holder.binding.ivShare.setColorFilter(iconColor, android.graphics.PorterDuff.Mode.MULTIPLY);
        holder.binding.ivFav.setColorFilter(iconColor, android.graphics.PorterDuff.Mode.MULTIPLY);


        val data = quotesList!![position]
        holder.binding.tvQuotes.text =
            HtmlCompat.fromHtml(data.quotes!!, HtmlCompat.FROM_HTML_MODE_LEGACY)
        holder.binding.tvAuthor.text = "- ${data.author}"




        holder.binding.ivCopy.setOnClickListener {
            listener.onCopyClick(data.quotes!!)
        }

        holder.binding.ivShare.setOnClickListener {
            listener.onShareClick(data.quotes!! + "\n\n-${data.author}")
        }

        holder.binding.ivFav.setOnClickListener {
            listener.onFavClick(holder.binding.ivFav, data.id!!)
        }


    }

    override fun getItemCount(): Int {
        return if (quotesList == null) 0 else quotesList!!.size
    }

    fun setList(list: List<QuotesModel>?) {
        this.quotesList = list
    }

    inner class Holder(val binding: LayoutRvListQuotesBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }


}