package com.example.quotes

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteException
import kotlin.Throws
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.example.quotes.adapter.QuotesModel
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.StringBuilder
import java.util.ArrayList

class QuotesDb(var context: Context?, name: String?) : SQLiteOpenHelper(
    context, name, null, 1
) {


    var dbName: String?
    fun checkDatabase(): Boolean {
        return try {
            File(path + dbName).exists()
        } catch (unused: SQLiteException) {
            false
        }
    }

    @Throws(IOException::class)
    fun copyDatabse() {
        val open = context!!.assets.open("$dbName")
        val file = File("$path$dbName")
        if (!file.parentFile.exists()) {
            file.parentFile.mkdir()
        }
        val fileOutputStream = FileOutputStream(file)
        val bArr = ByteArray(2024)
        while (true) {
            val read = open.read(bArr)
            if (read > 0) {
                fileOutputStream.write(bArr, 0, read)
            } else {
                fileOutputStream.flush()
                fileOutputStream.close()
                open.close()
                return
            }
        }
    }

    fun createDatabse() {
        if (!checkDatabase()) {
            try {
                copyDatabse()
            } catch (e: IOException) {
                Log.e(TAG, "mo19753k: ", e)
            }
        }
    }

    override fun onCreate(db: SQLiteDatabase) {

    }
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (newVersion > oldVersion) {
            val file = File(path + dbName)
            if (file.exists()) {
                file.delete()
                println("delete database file.")
            }
        }
    }


    val quotes: MutableList<QuotesModel>
        get() {
            val db = readableDatabase
            val list: MutableList<QuotesModel> = ArrayList()
            val cursor = db.query("Quotes", null, null, null, null, null, null)
            if (cursor.moveToFirst()) {
                do {
                    val model = QuotesModel()
                    val quote = cursor.getString(cursor.getColumnIndexOrThrow("quote"))
                    if (!quote.isNullOrEmpty()) {
                        model.quotes = quote
                        model.author = cursor.getString(cursor.getColumnIndexOrThrow("author"))
                        model.category = cursor.getString(cursor.getColumnIndexOrThrow("topic"))
                        model.fav = cursor.getInt(cursor.getColumnIndexOrThrow("favorite"))
                        list.add(model)
                    }


                } while (cursor.moveToNext())
            }
            return list
        }

    companion object {
        lateinit var path: String
        private const val TAG = "QuotesDb"
    }

    init {
        val builder = StringBuilder("/data/data/")
        builder.append(context!!.packageName).append("/databases/")
        path = builder.toString()
        dbName = name
    }
}